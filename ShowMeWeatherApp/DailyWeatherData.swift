//
//  DailyWeatherData.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 21.07.2022.
//

import Foundation

class DailyWeatherData {
    var cityName: String? = ""
    var countryName: String? = ""
    var lastUpdate: String? = ""
    var conditionInf: String? = ""
    var days: [Dayly]? = []
}

struct Dayly {
    var date: String = ""
    var temperature: Double? = 0.0
    var icon: Int? = 0
    var phrase: String? = ""
    var windSpeed: Double? = 0.0
    var airQualityValue: Int? = 0
    var airQualityCategory: String? = ""
}
