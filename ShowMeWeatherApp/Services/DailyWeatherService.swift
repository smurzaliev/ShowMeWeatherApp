//
//  NetworkManager.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 08.07.2022.
//

import Foundation
import Moya
import Alamofire

protocol DailyWeatherServiceProtocol {
    func getDailyWeather(location: String, _ competion: @escaping (Result<DailyWeatherModel, Error>) -> ())
}

struct DailyWeatherService: DailyWeatherServiceProtocol {
    
    var provider = NetworkProvider<DailyWeatherTarget>()
    
    func getDailyWeather(location: String, _ competion: @escaping (Result<DailyWeatherModel, Error>) -> ()) {
        provider.request(.loadDailyWeather(location: location), onSuccess: { response in
            do {
                let safeData = try JSONDecoder().decode(DailyWeatherModel.self, from: response.data)
                competion(.success(safeData))
            } catch let error {
                competion(.failure(error))
            }
        }, onFailure: { error in
            competion(.failure(error))
        })
    }
}
