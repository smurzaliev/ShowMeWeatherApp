//
//  Weather.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 08.07.2022.
//

import Foundation
import Moya

public enum DailyWeatherTarget {
    case loadDailyWeather(location: String)
}

extension DailyWeatherTarget: TargetType {
    
    public var baseURL: URL {
        let baseURL = "http://dataservice.accuweather.com"
        return URL(string: baseURL)!
    }
    
    public var path: String {
        switch self {
        case .loadDailyWeather(let location):
            return "/forecasts/v1/daily/5day/\(location)"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var task: Task {
        switch self {
        case .loadDailyWeather(location: _):
            return .requestParameters(parameters: [
                "apikey" : "YS7mx8TJj5n3s2q6Da5W3hv5BQD3ld9L",
                "language" : "en",
                "details" : "true",
                "metric" : "true"
            ], encoding: URLEncoding.default)
        }
    }

    public var headers: [String : String]? {
        return [:]
    }
}
