//
//  HourlyWeatherTarget.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 08.07.2022.
//

import Foundation
import Moya

public enum HourlyWeatherTarget {
    case loadHourlyWeather(location: String)
}

extension HourlyWeatherTarget: TargetType {
    
    public var baseURL: URL {
        let baseURL = "http://dataservice.accuweather.com"
        return URL(string: baseURL)!
    }
    
    public var path: String {
        switch self {
        case .loadHourlyWeather(let location):
            return "/forecasts/v1/hourly/12hour/\(location)"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var task: Task {
        switch self {
        case .loadHourlyWeather(location: _):
            return .requestParameters(parameters: [
                "apikey" : "YS7mx8TJj5n3s2q6Da5W3hv5BQD3ld9L",
                "language" : "en",
                "details" : "false",
                "metric" : "true"
            ], encoding: URLEncoding.default)
        }
    }

    public var headers: [String : String]? {
        return [:]
    }
}
