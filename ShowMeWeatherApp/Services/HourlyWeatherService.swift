//
//  HourlyWeatherService.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 18.07.2022.
//

import Foundation
import Moya
import Alamofire

protocol HourlyWeatherServiceProtocol {
    func getHourlyWeather(location: String, _ competion: @escaping (Result<HourlyWeatherModel, Error>) -> ())
}

struct HourlyWeatherService: HourlyWeatherServiceProtocol {
    
    var provider = NetworkProvider<HourlyWeatherTarget>()
    
    func getHourlyWeather(location: String, _ competion: @escaping (Result<HourlyWeatherModel, Error>) -> ()) {
        provider.request(.loadHourlyWeather(location: location), onSuccess: { response in
            do {
                let safeData = try JSONDecoder().decode(HourlyWeatherModel.self, from: response.data)
                competion(.success(safeData))
            } catch let error {
                competion(.failure(error))
            }
        }, onFailure: { error in
            competion(.failure(error))
        })
    }
}
