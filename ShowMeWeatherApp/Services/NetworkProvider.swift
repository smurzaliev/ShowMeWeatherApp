//
//  NetworkProvider.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 18.07.2022.
//

import Foundation
import Moya
import Alamofire

class NetworkProvider<Target: TargetType> {
    
    private let provider: MoyaProvider<Target>
    
    init() {
        provider = MoyaProvider(plugins: [NetworkLoggingPlugin()])
        
//        #if DEBUG
//            provider = MoyaProvider(plugins: [NetworkLoggingPlugin()])
//        #else
//            provider = MoyaProvider()
//        #endif
    }
    
    func request(_ target: Target,
                 onSuccess: @escaping (Response) -> Void,
                 onFailure: @escaping (Error) -> Void) {
       
        provider.request(target) { result in
            switch result {
            case .success(let response):
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
}

