////
////  example.swift
////  ShowMeWeatherApp
////
////  Created by Samat Murzaliev on 08.07.2022.
////
//
//import Foundation
//
//
//import Foundation
//import Moya
//
//protocol DetailNetworkProtocol {
//    func loadTopic(topicId: String, _ completion: @escaping (Result<TopicDetailsResponse, Error>) -> Void)
//}
//
//struct DetailService: DetailNetworkProtocol {
//    private let provider = NetworkProvider<DetailTarget>()
//
//    func loadTopic(topicId: String, _ completion: @escaping (Result<TopicDetailsResponse, Error>) -> Void) {
//
//        provider.request(.loadTopic(topicId), onSuccess: { (response) in
//            do {
//                let stringValue = String(decoding: response.data, as: UTF8.self)
//                print(stringValue)
//
//                let baseResp = try response.map(TopicDetailsResponse.self)
//                completion(.success(baseResp))
//
//            } catch {
//                do {
//                    let baseResp = try response.map(BaseResponse.self)
//                    completion(.failure(NSError(domain: .empty, code:0, userInfo:[ NSLocalizedDescriptionKey: baseResp.message ??  .empty])))
//                } catch {
//                    completion(.failure(error))
//                }
//            }
//        }) { (error) in
//            completion(.failure(error))
//        }
//    }
//}
//
//enum DetailTarget {
//    case loadTopic(_ topicId: String)
//}
//
//extension DetailTarget: TargetType {
//    var headers: [String : String]? {return [:]}
//
//    var path: String {
//        switch self {
//        case .loadTopic:
//            return "mobile.php"
//        }
//    }
//
//    var method: Moya.Method {
//        switch self {
//        case .loadTopic:
//            return .get
//        }
//    }
//
//    var task: Task {
//        switch self {
//        case .loadTopic:
//            return .requestParameters(parameters: requestParameters, encoding: URLEncoding.default)
//        }
//    }
//
//    var requestParameters: [String : Any] {
//        switch self {
//        case .loadTopic(let topicId):
//            return ["event" : "get_topic","id": topicId]
//        }
//    }
//}
//
//import Foundation
//import Moya
//import Alamofire
//
//class NetworkProvider<Target: TargetType> {
//
//    private let provider: MoyaProvider<Target>
//
//    init() {
//        provider = MoyaProvider(plugins: [NetworkLoggingPlugin()])
//
////        #if DEBUG
////             provider = MoyaProvider(plugins: [NetworkLoggingPlugin()])
////        #else
////             provider = MoyaProvider()
////        #endif
//    }
//
//    func request(_ target: Target,
//                 onSuccess: @escaping (Response) -> Void,
//                 onFailure: @escaping (Error) -> Void) {
//
//        provider.request(target) { result in
//            switch result {
//            case .success(let response):
//                onSuccess(response)
//            case .failure(let error):
//                onFailure(error)
//            }
//        }
//    }
//}
//
//extension TargetType {
//
//    public var sampleData: Data { Data() }
//
//    var baseURL: URL {
//        let stringURL = "https://kaktus.media/api/v1/"
//        return URL(string: stringURL)!
//    }
//}
//
//
