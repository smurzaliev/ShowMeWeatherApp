//
//  HomeViewModel.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 18.07.2022.
//

import Foundation

protocol HomeViewDelegate {
    func updateForecast()
}

class HomeViewModel {
    
    private let dailyWeatherService: DailyWeatherServiceProtocol
    private let hourlyWeatherService: HourlyWeatherServiceProtocol
    var selectedCityDailyWeather = DailyWeatherData()
    var selectedCityHourlyWeather: [HourlyWeatherData] = []
    var delegate: HomeViewDelegate!
    var defaultCityAvailable = false
    
    init(dailyWeatherService: DailyWeatherServiceProtocol, hourlyWeatherService: HourlyWeatherServiceProtocol) {
        self.dailyWeatherService = dailyWeatherService
        self.hourlyWeatherService = hourlyWeatherService
    }
    
    func fetchDailyWeather(city: String) {
        dailyWeatherService.getDailyWeather(location: city) { [weak self] result in
            switch result {
            case.success(let data):
                self?.saveWeatherData(data: data)
                self?.delegate.updateForecast()
            case .failure(let error):
                print("Error here \(error.localizedDescription)")
            }
        }
    }
    
    func fetchHourlyWeather(city: String) {
        hourlyWeatherService.getHourlyWeather(location: city) { [weak self] result in
            switch result {
            case .success(let data):
                self?.saveHourlyData(data: data)
                self?.delegate.updateForecast()
            case .failure(let error):
                print("Error here \(error.localizedDescription)")

            }
        }
    }
    
    private func saveHourlyData(data: HourlyWeatherModel) {
        for hourlyModel in data {
            let newItem = HourlyWeatherData()
            newItem.dateTime = hourlyModel.dateTime
            newItem.weatherIcon = hourlyModel.weatherIcon
            newItem.temperature = hourlyModel.temperature.value
            selectedCityHourlyWeather.append(newItem)
        }
    }
    
    private func saveWeatherData(data: DailyWeatherModel) {
        
        let hourFormatter = DateFormatter()
        hourFormatter.dateFormat = "HH:mm"
        let currentHour: String = hourFormatter.string(from: Date())
        selectedCityDailyWeather.lastUpdate = currentHour
        
        selectedCityDailyWeather.conditionInf = data.headline.text
        selectedCityDailyWeather.days?.append(Dayly())
        selectedCityDailyWeather.days?[0].date = data.dailyForecasts[0].date
        selectedCityDailyWeather.days?[0].temperature = data.dailyForecasts[0].temperature.maximum.value
        selectedCityDailyWeather.days?[0].icon = data.dailyForecasts[0].day.icon
        selectedCityDailyWeather.days?[0].phrase = data.dailyForecasts[0].day.iconPhrase
        selectedCityDailyWeather.days?[0].windSpeed = data.dailyForecasts[0].day.wind.speed.value
        selectedCityDailyWeather.days?[0].airQualityValue = data.dailyForecasts[0].airAndPollen[0].value
        selectedCityDailyWeather.days?[0].airQualityCategory = data.dailyForecasts[0].airAndPollen[0].category
        
        selectedCityDailyWeather.days?.append(Dayly())
        selectedCityDailyWeather.days?[1].date = data.dailyForecasts[1].date
        selectedCityDailyWeather.days?[1].temperature = data.dailyForecasts[1].temperature.maximum.value
        selectedCityDailyWeather.days?[1].icon = data.dailyForecasts[1].day.icon
        selectedCityDailyWeather.days?[1].phrase = data.dailyForecasts[1].day.iconPhrase
        selectedCityDailyWeather.days?[1].windSpeed = data.dailyForecasts[1].day.wind.speed.value
        selectedCityDailyWeather.days?[1].airQualityValue = data.dailyForecasts[1].airAndPollen[0].value
        selectedCityDailyWeather.days?[1].airQualityCategory = data.dailyForecasts[1].airAndPollen[0].category
        
        selectedCityDailyWeather.days?.append(Dayly())
        selectedCityDailyWeather.days?[2].date = data.dailyForecasts[2].date
        selectedCityDailyWeather.days?[2].temperature = data.dailyForecasts[2].temperature.maximum.value
        selectedCityDailyWeather.days?[2].icon = data.dailyForecasts[2].day.icon
        selectedCityDailyWeather.days?[2].phrase = data.dailyForecasts[2].day.iconPhrase
        selectedCityDailyWeather.days?[2].windSpeed = data.dailyForecasts[2].day.wind.speed.value
        selectedCityDailyWeather.days?[2].airQualityValue = data.dailyForecasts[2].airAndPollen[0].value
        selectedCityDailyWeather.days?[2].airQualityCategory = data.dailyForecasts[2].airAndPollen[0].category
        
        selectedCityDailyWeather.days?.append(Dayly())
        selectedCityDailyWeather.days?[3].date = data.dailyForecasts[3].date
        selectedCityDailyWeather.days?[3].temperature = data.dailyForecasts[3].temperature.maximum.value
        selectedCityDailyWeather.days?[3].icon = data.dailyForecasts[3].day.icon
        selectedCityDailyWeather.days?[3].phrase = data.dailyForecasts[3].day.iconPhrase
        selectedCityDailyWeather.days?[3].windSpeed = data.dailyForecasts[3].day.wind.speed.value
        selectedCityDailyWeather.days?[3].airQualityValue = data.dailyForecasts[3].airAndPollen[0].value
        selectedCityDailyWeather.days?[3].airQualityCategory = data.dailyForecasts[3].airAndPollen[0].category
        
        selectedCityDailyWeather.days?.append(Dayly())
        selectedCityDailyWeather.days?[4].date = data.dailyForecasts[4].date
        selectedCityDailyWeather.days?[4].temperature = data.dailyForecasts[4].temperature.maximum.value
        selectedCityDailyWeather.days?[4].icon = data.dailyForecasts[4].day.icon
        selectedCityDailyWeather.days?[4].phrase = data.dailyForecasts[4].day.iconPhrase
        selectedCityDailyWeather.days?[4].windSpeed = data.dailyForecasts[4].day.wind.speed.value
        selectedCityDailyWeather.days?[4].airQualityValue = data.dailyForecasts[4].airAndPollen[0].value
        selectedCityDailyWeather.days?[4].airQualityCategory = data.dailyForecasts[4].airAndPollen[0].category
    }
}
