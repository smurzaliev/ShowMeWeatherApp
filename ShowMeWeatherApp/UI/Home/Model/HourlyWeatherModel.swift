//
//  HourlyWeatherModel.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 25.07.2022.
//

import Foundation

// MARK: - HourlyWeatherModelElement
struct HourlyWeatherModelElement: Codable {
    let dateTime: String
    let weatherIcon: Int
    let temperature: Temperature

    enum CodingKeys: String, CodingKey {
        case dateTime = "DateTime"
        case weatherIcon = "WeatherIcon"
        case temperature = "Temperature"
    }
}

// MARK: - Temperature
struct Temperature: Codable {
    let value: Double

    enum CodingKeys: String, CodingKey {
        case value = "Value"
    }
}

typealias HourlyWeatherModel = [HourlyWeatherModelElement]
