//
//  DailyWeatherModel.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 08.07.2022.

import Foundation

// MARK: - DailyWeatherModel
struct DailyWeatherModel: Codable {
    let headline: Headline
    let dailyForecasts: [DailyForecast]

    enum CodingKeys: String, CodingKey {
        case headline = "Headline"
        case dailyForecasts = "DailyForecasts"
    }
}

// MARK: - DailyForecast
struct DailyForecast: Codable {
    let date: String
    let temperature: RealFeelTemperature
    let airAndPollen: [AirAndPollen]
    let day: Day

    enum CodingKeys: String, CodingKey {
        case date = "Date"
        case temperature = "Temperature"
        case airAndPollen = "AirAndPollen"
        case day = "Day"
    }
}

// MARK: - AirAndPollen
struct AirAndPollen: Codable {
    let value: Int
    let category: String

    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case category = "Category"
    }
}

// MARK: - Day
struct Day: Codable {
    let icon: Int
    let iconPhrase: String
    let wind, windGust: Wind

    enum CodingKeys: String, CodingKey {
        case icon = "Icon"
        case iconPhrase = "IconPhrase"
        case wind = "Wind"
        case windGust = "WindGust"
    }
}

// MARK: - Evapotranspiration
struct Evapotranspiration: Codable {
    let value: Double
    let unit: Unit

    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case unit = "Unit"

    }
}

enum Phrase: String, Codable {
    case hot = "Hot"
    case pleasant = "Pleasant"
    case quiteHot = "Quite Hot"
    case veryWarm = "Very Warm"
}

enum Unit: String, Codable {
    case c = "C"
    case cm = "cm"
    case kmH = "km/h"
    case mm = "mm"
    case wM = "W/m²"
}

// MARK: - Wind
struct Wind: Codable {
    let speed: Evapotranspiration

    enum CodingKeys: String, CodingKey {
        case speed = "Speed"
    }
}


// MARK: - RealFeelTemperature
struct RealFeelTemperature: Codable {
    let minimum, maximum: Evapotranspiration

    enum CodingKeys: String, CodingKey {
        case minimum = "Minimum"
        case maximum = "Maximum"
    }
}

// MARK: - Headline
struct Headline: Codable {
    let effectiveDate: String
    let text, category: String

    enum CodingKeys: String, CodingKey {
        case effectiveDate = "EffectiveDate"
        case text = "Text"
        case category = "Category"
    }
}
