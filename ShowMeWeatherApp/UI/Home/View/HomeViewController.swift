//
//  HomeViewController.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 04.07.2022.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var weatherInfoView: WeatherInfoView!
    
    @IBOutlet weak var hourlyForecast: UICollectionView!
    
    @IBOutlet weak var informationState: InformationStateView!
    
    @IBOutlet weak var dailyForecastTable: UITableView!
    
    @IBOutlet weak var locationLabel: UILabel!
        
    @IBOutlet weak var hourlyLabel: UILabel!
    @IBOutlet weak var dailyLabel: UILabel!
    private let viewModel = HomeViewModel(dailyWeatherService: DailyWeatherService(), hourlyWeatherService: HourlyWeatherService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        setupSubviews()

        if viewModel.defaultCityAvailable {
            viewModel.fetchDailyWeather(city: "328328")
            viewModel.fetchHourlyWeather(city: "328328")
        } else {
            weatherInfoView.isHidden = true
            hourlyForecast.isHidden = true
            informationState.isHidden = true
            dailyForecastTable.isHidden = true
            locationLabel.text = "Add City"
            hourlyLabel.text = "No Weather Data available. Please choose city first."
            dailyLabel.isHidden = true
        }
//        viewModel.fetchDailyWeather(city: "222844")
        
        
    }
    
    private func setupSubviews() {
        hourlyForecast.delegate = self
        hourlyForecast.dataSource = self
        hourlyForecast.backgroundColor = .white
        hourlyForecast.register(UINib(nibName: "HourlyForecastViewCell", bundle: .main), forCellWithReuseIdentifier: "HourCell")
        
        dailyForecastTable.delegate = self
        dailyForecastTable.dataSource = self
        dailyForecastTable.backgroundColor = .white
        dailyForecastTable.register(UINib(nibName: "DailyForecastCell", bundle: .main), forCellReuseIdentifier: "DailyCell")

    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.selectedCityHourlyWeather.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourCell", for: indexPath) as! HourlyForecastViewCell
        let index = indexPath.item
        cell.fill(data: viewModel.selectedCityHourlyWeather[index])
        cell.timeLable.text = "\(6 + index):00"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 78, height: 107)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel.selectedCityDailyWeather.days!.count - 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyCell", for: indexPath) as! DailyForecastCell
        let index = indexPath.row
        cell.fill(data: (viewModel.selectedCityDailyWeather.days?[ index + 1])!, index: index)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension HomeViewController: HomeViewDelegate {
    func updateForecast() {
        weatherInfoView.tempLabel.text = "\(viewModel.selectedCityDailyWeather.days?.first?.temperature! ?? 0)° C"
        weatherInfoView.weatherDescription.text = viewModel.selectedCityDailyWeather.days?.first?.phrase
        weatherInfoView.weatherIcon.image = UIImage(named: "\(viewModel.selectedCityDailyWeather.days?.first?.icon! ?? 01)-s")
        informationState.weatherCondition.text = viewModel.selectedCityDailyWeather.conditionInf?.description
        dailyForecastTable.reloadData()
        hourlyForecast.reloadData()
        weatherInfoView.lastUpdateLabel.text = viewModel.selectedCityDailyWeather.lastUpdate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "EN")
        let currentDate: String = dateFormatter.string(from: Date())
        weatherInfoView.dateLabel.text = "\(currentDate)"
       
        let hourFormatter = DateFormatter()
        hourFormatter.dateFormat = "HH:mm"
        let currentHour: String = hourFormatter.string(from: Date())
        weatherInfoView.timeLabel.text = "\(currentHour)"
    }
}
