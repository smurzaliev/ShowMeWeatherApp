//
//  DetailViewController.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 21.07.2022.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailWeather: DetailWeatherTop!
    
    @IBOutlet weak var hourlyCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupSubview()
    }
    
    @objc func backPressed(view: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    private func setupSubview() {
        detailWeather.backButton.addTarget(self, action: #selector( backPressed(view:)), for: .touchUpInside)
        hourlyCollection.backgroundColor = .white
        hourlyCollection.delegate = self
        hourlyCollection.dataSource = self
        hourlyCollection.register(UINib(nibName: "HourlyForecastViewCell", bundle: .main), forCellWithReuseIdentifier: "HourCell")
    }

}

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourCell", for: indexPath) as! HourlyForecastViewCell
        cell.tempLabel.text = "33"
        cell.weatherIcon.image = UIImage(systemName: "cloud.rain.fill")
        cell.timeLable.text = "4.00 PM"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 78, height: 107)
    }
}
