//
//  HourlyWeatherData.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 25.07.2022.
//

import Foundation

class HourlyWeatherData {
    var dateTime: String = ""
    var weatherIcon: Int = 0
    var temperature: Double = 0.0
}
