//
//  ExtentionUtils.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 18.07.2022.
//

import Foundation

extension String {
    
    static var empty: String { return "" }
    
    static var newLine: String { return "\n" }
    
    static func whitespace(count: Int = 1) -> String {
        return String(repeating: " ", count: count)
    }
}
