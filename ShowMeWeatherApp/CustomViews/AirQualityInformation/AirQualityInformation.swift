//
//  AirQualityInformation.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 21.07.2022.
//

import UIKit

class AirQualityInformation: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        let addView = Bundle.main.loadNibNamed("AirQualityInformation", owner: self, options: nil)?.first as? UIView
        addView?.frame = self.bounds
        addSubview(addView!)
        addView?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
