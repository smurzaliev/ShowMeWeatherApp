//
//  WeatherInfoView.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 04.07.2022.
//

import UIKit

class WeatherInfoView: UIView {
    
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var lastUpdateLabel: UILabel!
    
    @IBOutlet weak var weatherDescription: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        let addView = Bundle.main.loadNibNamed("WeatherInfoView", owner: self, options: nil)?.first as? UIView
        addView?.frame = self.bounds
        addSubview(addView!)
        addView?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
    
    
}
