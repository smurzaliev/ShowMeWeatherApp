//
//  InformationStateView.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 18.07.2022.
//

import UIKit

class InformationStateView: UIView {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var weatherCondition: UILabel!
    
    @IBOutlet weak var suggestionLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        let addView = Bundle.main.loadNibNamed("InformationStateView", owner: self, options: nil)?.first as? UIView
        addView?.frame = self.bounds
        addSubview(addView!)
        addView?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
}
