//
//  DetailWeatherTop.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 20.07.2022.
//

import UIKit

class DetailWeatherTop: UIView {

    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    @IBOutlet weak var lastUpdateLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var menuButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        let addView = Bundle.main.loadNibNamed("DetailWeatherTop", owner: self, options: nil)?.first as? UIView
        addView?.frame = self.bounds
        addSubview(addView!)
        addView?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
