//
//  HourlyForecastViewCell.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 07.07.2022.
//

import UIKit

class HourlyForecastViewCell: UICollectionViewCell {

    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var timeLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(data: HourlyWeatherData) {
        weatherIcon.image = UIImage(named: "\(data.weatherIcon)-s")
        tempLabel.text = "\(data.temperature)° C"
    }
}
