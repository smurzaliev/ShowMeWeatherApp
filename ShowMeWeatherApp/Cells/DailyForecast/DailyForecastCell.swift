//
//  DailyForecastCellTableViewCell.swift
//  ShowMeWeatherApp
//
//  Created by Samat Murzaliev on 19.07.2022.
//

import UIKit

class DailyForecastCell: UITableViewCell {

    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var weekDayLabel: UILabel!
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    @IBOutlet weak var tempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func fill(data: Dayly, index: Int) {
        weatherLabel.text = data.phrase
        tempLabel.text = "\(data.temperature!)° C"
        weekDayLabel.text = data.date
        self.backgroundColor = UIColor(red: 210, green: 223, blue: 255, alpha: 1)
        weatherIcon.image = UIImage(named: "\(data.icon!)-s")
        
        let dateFormatter = DateFormatter()
        let date = Calendar.current.date(byAdding: DateComponents(day: index + 1), to: Date())!
        dateFormatter.dateFormat = "EEEE"
        dateFormatter.locale = Locale(identifier: "EN")
        let currentDate: String = dateFormatter.string(from: date)
        weekDayLabel.text = "\(currentDate)"
    }
    
}
